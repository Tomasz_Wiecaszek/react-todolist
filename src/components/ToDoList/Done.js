
import {React} from 'react'
import './style.css'
import useFormFunction from './useFormFunctions'

const Done=({index,state,setState,ifDone})=>{
    const [,,changeColor]=useFormFunction();

    return(<>
        <input type="checkbox" checked={ifDone} onChange={()=>{changeColor(index,state,setState)}}/>
    
        </>
    )
}
export default Done;