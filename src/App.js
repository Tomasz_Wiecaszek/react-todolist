import {BrowserRouter as Router, Switch,Route} from 'react-router-dom';
import './App.css';
import Homepage from './pages/homepage';
import {GameApp} from './context'
import Nav from './components/Nav'
import User from './pages/user'
import Game from './pages/game'
import Rectangle from './components/rectangle'
import Circle from './components/Circle'
import MyHook from './components/MyHook'
import ToDoList from './components/ToDoList'

function App() {

  return (
    <div className="mainContainer">
      <Router>
  <GameApp>
    {/* <Nav/> */}
      <Switch>
      <Route exact path="/rectangle" component={Rectangle}/>
      <Route exact path="/circle" component={Circle}/>
      <Route exact path="/hook" component={MyHook}/>
      <Route exact path="/ToDoList" component={ToDoList}/>
        <Route exact path="/" component={Homepage}/>
        <Route  path="/user" component={User}/>
        <Route  path="/game" component={Game}/>

        
      </Switch>
  </GameApp>
      </Router>
    </div>
  );
}

export default App;
