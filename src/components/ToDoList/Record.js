
import {React, useEffect, useState,} from 'react'
import './style.css'
import Done from './Done'
import Delete from './Delete'
import useFormFunction from './useFormFunctions'

const Record =({list,setList, changeColor})=>{


    return(
    <>
    <ul>
    <li>
                           <table className='recordRow' style={{backgroundColor:"black", color:"white"}}>
                   
                       <tbody><tr>
                              <td className="index">LP.</td>
                              <td className="task">Zadanie</td>
                              <td className="data">Data wykonania</td>
                              <td className="done">Czy wykonano?</td>
                              <td className="delete">Usuń  z listy</td>
                    </tr>
                    </tbody></table> 
                </li>

        {list.map((el)=>{return(
            
                <li key={Math.random()}>
                   <table className='recordRow' style={{backgroundColor:el.ifDone?"rgb(0, 219, 91)":"rgb(129, 240, 255)"}}>
                   
                       <tbody><tr>
                              <td className="index">{el.index}</td>
                              <td className="task">{el.task}</td>
                              <td className="data">{el.date}</td>
                              <td className="done"><Done ifDone={el.ifDone} index={el.index} state={list} setState={setList}/></td>
                              <td className="delete"><Delete state={list} setState={setList} index={el.index}/></td>
                    </tr>
                    </tbody></table> 
                </li>
        )})}
        </ul>
            
    

    </>)
}

export default Record;