import React from 'react'
import ProgressBar from '../ProgressBar'
const TableRows = props =>{
    return(
        <React.Fragment>
        <table>
        <tbody>
        <tr><td>Name : {props.name}</td></tr>
        <tr><td>Strong : {props.str}</td></tr>
        <tr><td>Speed : {props.speed}</td></tr>
        <tr><td>HP : {props.hp}</td></tr>
       
        <tr><td>Damage : {props.dmg}</td></tr>
        </tbody>
        </table> {!props.alive?
        <ProgressBar lvl={props.lvl} />
        :<div></div>}
        </React.Fragment >
    )
}
export default TableRows;