import './style.css'
import {React,useEffect,useRef, useState} from 'react'
import Ball from './Ball'

const Circle =()=>{


    const maxTop=180;
    let [par,setPar]=useState(getRandom(0,10));
    let [maxLeft,setMaxLeft]=useState(580);
    let [direct,setDirect]=useState("");
    let [minLeft,setMinLeft]=useState(0);
    let [topPos,setTopPos]=useState(0);
    let [leftPos,setLeftPos]=useState(0);
    let [color,setColor]=useState("blue");
    let [delay,setDelay]=useState(1000);

function getRandom(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
      }


    
    const colors=[
        ['red','blue'],
        ['green','yellow'],
        ['black','pink'],
        ['grey','violet'],
        ['green','black'],
        ['blue','pink'],
        ['grey','blue'],
        ['violet','green'],
        ['black','yellow'],
    ['blue','yellow'],
    ];



    

      function useInterval(callback, delay) {
        const savedCallback = useRef();
      
        // Remember the latest callback.
        useEffect(() => {
          savedCallback.current = callback;
        }, [callback]);
      
        // Set up the interval.
        useEffect(() => {
          function tick() {
            savedCallback.current();
          }
          if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
          }
        }, [delay]);
      }

      const start=()=>{
        setDirect("right");
      }
      const stop=()=>{
        setDirect("");
      }
    const randomPos=()=>{
        
        if(direct=="right"){
        setTopPos(()=>getRandom(0,maxTop));
        setLeftPos(()=>getRandom(minLeft,580));
        setMinLeft(()=>minLeft=leftPos);
            if(minLeft>578){
                setDirect('left'); 
                setColor(colors[par][0]) ;
            }
        
    }
        else if(direct=='left'){
        setTopPos(()=>getRandom(0,maxTop));
        setLeftPos(()=>getRandom(0,maxLeft));
        setMaxLeft(()=>maxLeft=leftPos);
        if(maxLeft<2){
            setDirect('right');  
            setColor(colors[par][1])
        }
        }
        else{console.log("Start this")}
        console.log("Left: "+leftPos)

        }
        useInterval(randomPos,delay)
    
        const changeColors=()=>{

          if(color==colors[par][0]){
            setColor(colors[par][1])
          }else{setColor(colors[par][0])}

        }
        const plusDelay=()=>{
          setDelay(delay+100);
        }
        const minusDelay=()=>{
          setDelay(delay-100);
        }
 

    return(<div>
        <div className="mainContainerr"><Ball top={topPos} left={leftPos} color={color}/> </div>
            <div  className="buttons">
            <button onClick={start}>START</button><button onClick={stop}>Stop</button><button onClick={changeColors}>Change Color</button>
            <button onClick={plusDelay}>+ Delay</button><button onClick={minusDelay}>- Delay</button>
            <p>Delay:  {delay}</p>
            </div>
            </div>
       
    )
}

export default Circle;

//IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
