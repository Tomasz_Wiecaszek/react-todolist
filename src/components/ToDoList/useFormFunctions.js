

const useFormFunction=()=>{

    const addRecord=(task='default',date='2000-02-02',ifDone=false,counter=0,setCounter,state,setState)=>{
        setCounter(++counter)
        let inTab=[{'index':counter,'task':task, 'date':date,'ifDone':ifDone}]
        setState(state=state.concat(inTab));
        
        }
        
        const deleteRecord=(numb,state,setState)=>{
            if(window.confirm("Czy napewno chcesz usunąć zadanie?")){
        let filtered = state.filter(function(el) { return el.index != numb; }); 
        setState(filtered);
        console.log(filtered);}
        
        }
        
        const changeColor=(numb,state,setState)=>{
        let newArray=state.slice();
        for(let i in state){
        if(state[i].index==numb){
        newArray[i].ifDone==false?newArray[i].ifDone=true:newArray[i].ifDone=false;
        }}
        setState(state=newArray)
            }
            return [addRecord,deleteRecord,  changeColor];

}

export default useFormFunction;