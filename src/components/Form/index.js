import React,{useContext,useState} from "react";
import {GameState} from "../../context"



const Form=()=>{
const {isName,setName}=useContext(GameState);
const [name,addName]=useState('');
const handleSubmit=(e)=>{
    if(name.length<10&&name.length>3){
    e.preventDefault();
    setName(true);
    localStorage.setItem("name",name);}
    else{alert("Your nick must have between 3 and 10 characters")}
}
return(
<form onSubmit={e=>handleSubmit(e)}>
    <p>WITAMY W GRZE, PROSZĘ PODAJ SWÓJ NICK, NICK MOŻE SKŁADAĆ SIĘ Z MAKSYMALNIE 10 ZNAKÓW.</p>
    <label>Imię: </label>
<input name="name" type="text" value={name} onChange={(e)=>addName(e.target.value)} />
<input name="send" type="submit"/>
</form>

)

}

export default Form;