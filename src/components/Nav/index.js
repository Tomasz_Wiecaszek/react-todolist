import {Link} from 'react-router-dom'
import './style.css'

const Nav=()=>{
    return(
        <div className="NavWrapper">
<ul>
    <li>
        <Link to="/" className="link">Home</Link>
    </li>
    <li>
        <Link to="/user" className="link">Postać</Link>
    </li>
    <li>
        <Link to="/game" className="link">Świat gry</Link>
    </li>
</ul>

        </div>
    )
}

export default Nav;