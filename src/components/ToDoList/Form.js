
import {React,useState} from 'react'
import useFormFunction from './useFormFunctions'


const Form =({state,setState})=>{
let [counter,setCounter]=useState(0);
let [task,setTask]=useState('');
let [date,setDate]=useState('')

const [addRecord,,]=useFormFunction();

const handleSubmit=(e)=>{
    if(task.length>3&&date.length>1){
e.preventDefault();
setCounter(()=>counter+1);
addRecord(task,date,false,counter,setCounter,state,setState);}
else{alert("Podaj prawidłowe dane.")}
}

    return(
        <div>
        <form onSubmit={e=>handleSubmit(e)}>
            <label htmlFor="task">Podaj nazwę zadania: </label>
        <input name='task' type='text' value={task} onChange={(e)=>setTask(e.target.value)}></input>
        <label htmlFor="date">Podaj datę zadania: </label>
        <input name='date' type='date' value={date} onChange={(e)=>setDate(e.target.value)}></input>
        <input type='submit' value='Dodaj task'/>
        </form>
        </div>
    )
}

export default Form;