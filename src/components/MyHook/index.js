import {React, useReducer, useState} from 'react'



const MyHook=()=>{

const startState={count:0,clicker:0};

const reducer = (state,action)=>{
switch(action.type){
    case 'incr':
        return( {count:state.count+1,clicker:state.clicker+1})
    case 'decr':
        return(alert("Odjeto"), {count:state.count-1,clicker:state.clicker+1})
    default:
        throw new Error()
}
}


const [state,dispatch]=useReducer(reducer,startState);

 return(
     <div>
<button onClick={()=>dispatch({type:'incr'})}>Dodaj</button>
<button onClick={()=>dispatch({type:'decr'})}>Odejmij</button>
<h1>Licznik: {state.count}   Kliknięto: {state.clicker} razy</h1>
     </div>
 )   
}

export default MyHook;