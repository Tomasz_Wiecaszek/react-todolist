
import Form from './Form'
import Record from './Record'
import useFormFunction from './useFormFunctions'

import {React,useEffect,useState} from 'react'

const ToDoList =()=>{

let [list,setList]=useState([]);
let [counter,setCounter]=useState(0);


return(

    <div>
        <Form state={list} setState={setList}/>
        <Record list={list} setList={setList}/>
    </div>
)
}

export default ToDoList;